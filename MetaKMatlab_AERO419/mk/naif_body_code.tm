KPL/MK
\begintext

Here is the additional NAIF ID defined for Hayabusa2 project

\begindata
 NAIF_BODY_NAME  += ('DSS-38', 'DSS-48', 'USUDA', 'UCHINOURA')
 NAIF_BODY_CODE  += ( 399038,   399048,  399048,      399038 )

 NAIF_BODY_NAME  += ('DSS-69', 'DSS-49', 'DSS-29', 'CEBREROS', 'NEW-NORCIA', 'MALARGUE')
 NAIF_BODY_CODE  += ( 399069,   399049,   399029,     399069,       399049,     399029 )

 NAIF_BODY_NAME  += ('CEB', 'NNO', 'MLG')
 NAIF_BODY_CODE  += (399069, 399049, 399029 )


 NAIF_BODY_NAME  += ('SPACECRAFT','RYUGU')
 NAIF_BODY_CODE  += (        -999, 2162173)
