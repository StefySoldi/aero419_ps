function loadspice_MetaK(path)

cspice_furnsh([path,'lsk/naif0012.tls']);
cspice_furnsh([path,'spk/de430.bsp']);
cspice_furnsh([path,'spk/mar097.bsp']);
cspice_furnsh([path,'spk/jup310.bsp']);
cspice_furnsh([path,'spk/2015094.bsp']);
cspice_furnsh([path,'spk/2003548.bsp']);
cspice_furnsh([path,'spk/2021900.bsp']);
cspice_furnsh([path,'spk/2011351.bsp']);
cspice_furnsh([path,'pck/pck00010.tpc']);
cspice_furnsh([path,'pck/gm_de431.tpc'])
cspice_furnsh([path,'mk/naif_body_code.tm']);
cspice_furnsh([path,'fk/sun_earth_rot.tf'])
cspice_furnsh([path,'fk/sun_jup_rot.tf'])


